package com.example.glory.myapp.s03;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Glory on 3/15/2018.
 */

public class MainAdapter extends BaseAdapter {
    Context mContext;
    List<MainItemModel> items;

    public MainAdapter(Context mContext, List<MainItemModel> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.gridview_main, parent,false);
        TextView name = v.findViewById(R.id.name);
        ImageView img = v.findViewById(R.id.img);
        name.setText(items.get(position).getName());
        Picasso.get().load(items.get(position).imgUrl).into(img);
        return v;
    }
}
