package com.example.glory.myapp.s03;

/**
 * Created by Glory on 3/16/2018.
 */

public class MainItemModel {
    String name;
    String imgUrl;

    public MainItemModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
