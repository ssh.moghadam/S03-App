package com.example.glory.myapp.s03;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.orhanobut.hawk.Hawk;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtUsername;
    EditText txtPassword;
    AppCompatCheckBox chkRememberMe;
    Button btnLogin;
    TextView txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Hawk.init(this).build();
        bindView();
        btnLogin.setOnClickListener(this);
    }

    public void bindView() {
        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        chkRememberMe = findViewById(R.id.chkRememberMe);
        btnLogin = findViewById(R.id.btnLogin);
        txtError = findViewById(R.id.txtError);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLogin) {
            if (txtUsername.getText().toString().trim().matches("") || txtPassword.getText().toString().trim().matches("")) {
                txtError.setText("Please Enter Usename|Password");
            } else {

                checkRememberPassword();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                String name = txtUsername.getText().toString();
                intent.putExtra("username",name);
                startActivity(intent);
                finish();
            }
        }
    }

    public void checkRememberPassword() {
        if (chkRememberMe.isChecked()) {
            Hawk.put("username", txtUsername.getText());
            Hawk.put("password", txtPassword.getText());
        } else {
            Hawk.delete("username");
            Hawk.delete("password");
        }

    }
}


