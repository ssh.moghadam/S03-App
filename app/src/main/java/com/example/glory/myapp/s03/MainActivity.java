package com.example.glory.myapp.s03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvName;
    Button btnLoginRegister;
    ListView lvMenu;
    GridView gvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        btnLoginRegister.setOnClickListener(this);

    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        if (i.getStringExtra("username") != null) {
            tvName.setText("Welcome " + i.getStringExtra("username"));
            btnLoginRegister.setVisibility(View.INVISIBLE);
        }


    }

    public void bindView() {
        tvName = findViewById(R.id.tvName);
        btnLoginRegister = findViewById(R.id.btnLoginRegister);
        fillListView();
        fillGridView();
    }

    public void fillListView() {
        lvMenu = findViewById(R.id.lvMenu);
        String menuItem[] = {"Home", "Setting", "About"};
        MenuAdapter menu_adapter = new MenuAdapter(this, menuItem);
        lvMenu.setAdapter(menu_adapter);
    }

    public void fillGridView() {
        gvMain = findViewById(R.id.gvMain);

        List<MainItemModel> items = new ArrayList<MainItemModel>();

        MainItemModel item1 = new MainItemModel();
        item1.setName("Yahoo");
        item1.setImgUrl("https://www.computing.co.uk/w-images/450b47c9-2f7e-439e-bec6-8fedfc5e1664/0/yahoologo2013-580x358.jpg");
        items.add(item1);

        MainItemModel item2 = new MainItemModel();
        item2.setName("Google");
        item2.setImgUrl("https://yt3.ggpht.com/a-/AJLlDp0TFaxkKTbr1YMaEdj0KOLllMoFJcuWOIm4XA=s900-mo-c-c0xffffffff-rj-k-no");
        items.add(item2);

        MainItemModel item3 = new MainItemModel();
        item3.setName("Instagram");
        item3.setImgUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjNrk9MeSDIp1gHDiJXQ3saFL2D8wOdSPEBKSItxR0U8Cnogn0");
        items.add(item3);

        MainItemModel item4 = new MainItemModel();
        item4.setName("Telegram");
        item4.setImgUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMwxjCLIUOLiXIuiFT8G7T27ZR93y214bFlWZ4qVqlVBIGe8uNHg");
        items.add(item4);

        MainItemModel item5 = new MainItemModel();
        item5.setName("Linkedin");
        item5.setImgUrl("https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/60/ac/da/60acdaaf-823b-3fb8-401a-62a602b67b1a/contsched.xqqmoatm.png/246x0w.png");
        items.add(item5);

        MainItemModel item6 = new MainItemModel();
        item6.setName("Line");
        item6.setImgUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6KaqLGD5019dCGdNH6AFzvQbubbZ16g3T0iazl3tUIUrDcdW_");
        items.add(item6);

        MainItemModel item7 = new MainItemModel();
        item7.setName("Skype");
        item7.setImgUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkHZBXnyKZpjTPE-YkO2TQIIkuEE5Fjw-MRb2z6ozo-clNFjTADw");
        items.add(item7);

        MainAdapter adapter2 = new MainAdapter(this, items);
        gvMain.setAdapter(adapter2);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLoginRegister) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);

        }
    }

}
