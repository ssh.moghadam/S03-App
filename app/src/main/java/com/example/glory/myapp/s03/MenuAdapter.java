package com.example.glory.myapp.s03;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Glory on 3/16/2018.
 */

public class MenuAdapter extends BaseAdapter {
    Context mContext;
    String menu_item[];

    public MenuAdapter(Context mContext, String[] menu_item) {
        this.mContext = mContext;
        this.menu_item = menu_item;
    }

    @Override
    public int getCount() {
        return menu_item.length;
    }

    @Override
    public Object getItem(int position) {
        return menu_item[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.listview_menu_item, parent, false);
        TextView menu = row.findViewById(R.id.menu_item);
        menu.setText(menu_item[position]);
        return row;
    }
}
